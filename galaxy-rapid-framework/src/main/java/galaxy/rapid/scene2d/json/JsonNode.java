package galaxy.rapid.scene2d.json;

import java.util.ArrayList;
import java.util.List;

public class JsonNode {

	public ComponentType type;
	public String name;
	public List<String> childrens = new ArrayList<String>();
	public String text;
	
}
