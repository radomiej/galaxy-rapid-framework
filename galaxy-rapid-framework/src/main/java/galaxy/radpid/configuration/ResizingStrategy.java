package galaxy.radpid.configuration;

public enum ResizingStrategy {
	WITHOUT_RESIZING, HALF, FLOAT;
}
