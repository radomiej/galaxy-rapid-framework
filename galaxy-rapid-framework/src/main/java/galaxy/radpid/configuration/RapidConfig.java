package galaxy.radpid.configuration;

public class RapidConfig {
	public String appName = "NoNameApp";
	public int appVersion = 0;
	public boolean multipleAsset;
	public boolean debugMode;
	public String skinAsset;
	public int defaultAssetScale = 1;
}
